package rangeset

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func BenchmarkRangeSetRandomAdd(b *testing.B) {
	b.StopTimer()

	fileSize := int64(10) * 1024 * 1024 * 1024 // 10GB
	maxSize := int64(1) * 1024 * 1024          // 1MB
	set := New(2000)

	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		offset := r.Int63n(fileSize)
		size := r.Int63n(maxSize)

		set.AddOffsetSize(offset, size, true)
	}

	b.StopTimer()
}

func BenchmarkRangeSetRandomRemove(b *testing.B) {
	b.StopTimer()

	fileSize := int64(10) * 1024 * 1024 * 1024 // 10GB
	maxSize := int64(1) * 1024 * 1024          // 1MB
	set := New(2000)
	set.AddOffsetSize(0, fileSize, false)

	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		offset := r.Int63n(fileSize)
		size := r.Int63n(maxSize)

		set.RemoveOffsetSize(offset, size, true)
	}

	b.StopTimer()
}

func TestRangeSetFullEmpty(t *testing.T) {
	size := int64(1024)
	set := New(size)
	back := size - 1

	_, err := set.Add(0, back, true)
	if err != nil {
		t.Fatal("failed to add.", err)
	}

	if err := set.Remove(0, back, false); err != nil {
		t.Fatal("failed to remove.", err)
	}

	if !set.IsEmpty() {
		t.Fatal("not empty")
	}
}

func TestRangeSetMerge(t *testing.T) {
	size := int64(1024)
	set := New(size)
	var err error

	// 영역 추가: 3-5
	_, err = set.Add(3, 5, true)
	if err != nil {
		t.Fatal("failed to add 3-5.", err)
	}

	// 이전 영역에 병합: 4-7
	_, err = set.Add(4, 7, true)
	if err != nil {
		t.Fatal("failed to add 4-7.", err)
	}

	// 병합 확인: 3-7
	arr := set.Array()
	if arr[0].Front() != 3 || arr[0].Back() != 7 {
		t.Fatal("wrong result 3-7.", arr[0].Front(), arr[0].Back())
	}

	// 겹치는 영역 추가 실패 테스트: 5-8
	_, err = set.Add(5, 8, false)
	if err == nil {
		t.Fatal("overlapped range added")
	}

	// 뒷부분에 영역 추가: 10-11
	_, err = set.Add(10, 11, false)
	if err != nil {
		t.Fatal("failed to add 10-12", err)
	}

	// 뒷부분에 영역 추가: 13-15
	_, err = set.Add(13, 15, false)
	if err != nil {
		t.Fatal("failed to add 13-15", err)
	}

	// 앞/뒤 병합 테스트: 5-20
	_, err = set.Add(5, 20, true)
	if err != nil {
		t.Fatal("failed to add 5-20.", err)
	}

	// 병합 확인: 3-20
	arr = set.Array()
	if arr[0].Front() != 3 || arr[0].Back() != 20 {
		t.Fatal("wrong result 3-20", arr[0].Front(), arr[0].Back())
	}
}

func TestRangeSetCut(t *testing.T) {
	size := int64(1024)
	set := New(size)
	var err error

	// 영역 추가: 3-5
	set.Add(3, 5, false)
	set.Add(10, 15, false)
	set.Add(20, 30, false)

	// 삭제: 20-30
	if err = set.Remove(20, 30, false); err != nil {
		t.Fatal("failed to remove 20-30", err)
	}
	if err = set.Remove(10, 20, false); err == nil {
		t.Fatal("wrong action. remove 10-20")
	}

	// 삭제: 3-15
	if err = set.Remove(3, 15, true); err != nil {
		t.Fatal("failed to remove 3-15", err)
	}

	set.Add(3, 5, false)
	set.Add(10, 15, false)
	set.Add(20, 30, false)

	// 삭제: 4-25
	if err = set.Remove(4, 25, true); err != nil {
		t.Fatal("failed to remove 4-25", err)
	}
}

func printRangeSet(set *RangeSet) {
	for _, r := range set.Array() {
		fmt.Println(r.Front(), "-", r.Back())
	}
	fmt.Println("range count:", set.Len())
}
