package rangeset

import "errors"

// 오류 목록
var (
	ErrOverlapped = errors.New("overlapped")
	ErrNotFound   = errors.New("not found")
)
