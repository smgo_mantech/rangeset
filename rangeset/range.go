package rangeset

// Range -- 범위
type Range struct {
	front, back int64
	// Offset int64
	// Size   int64
	Data interface{}
}

func newRange(front, back int64) *Range {
	return &Range{
		front: front,
		back:  back,
	}
}

func newRangeFromOffsetSize(offset, size int64) *Range {
	return &Range{
		front: offset,
		back:  offset + size - 1,
	}
}

// Front -- 맨 앞
func (r Range) Front() int64 {
	return r.front
}

// Back -- 맨 뒤
func (r Range) Back() int64 {
	return r.back
}

// Offset -- 오프셋
func (r Range) Offset() int64 {
	return r.front
}

// Size -- 길이
func (r Range) Size() int64 {
	return r.back - r.front + 1
}
