package rangeset

import (
	"strconv"
	"strings"
)

// RangeSet -- 영역 관리를 위한 컨테이너
type RangeSet struct {
	slice rangeSlice
}

// New -- 생성
func New(capacity int64) *RangeSet {
	return &RangeSet{
		slice: make([]*Range, 0, capacity),
	}
}

// IsEmpty -- 영역이 비어있는지 확인
func (s RangeSet) IsEmpty() bool {
	return s.slice.Len() == 0
}

// Len -- 영역 개수 반환
func (s RangeSet) Len() int {
	return s.slice.Len()
}

// Array -- 배열 반환
func (s RangeSet) Array() []*Range {
	return s.slice
}

// Clear -- 비우기
func (s RangeSet) Clear() {
	s.slice = s.slice[:0]
}

// AddOffsetSize -- 영역 추가
func (s *RangeSet) AddOffsetSize(offset, size int64, merge bool) (r *Range, err error) {
	return s.Add(offset, offset+size-1, merge)
}

// RemoveOffsetSize -- 영역 제거
func (s *RangeSet) RemoveOffsetSize(offset, size int64, force bool) error {
	return s.Remove(offset, offset+size-1, force)
}

// Add -- 영역 추가
func (s *RangeSet) Add(front, back int64, merge bool) (r *Range, err error) {
	// fmt.Println("add", front, "-", back)
	nextIndex, length := s.slice.search(front) // 같거나 다음 위치의 영역 찾기
	if nextIndex < length {
		r = s.slice[nextIndex]
		// 다음 영역이 존재 함
		if back >= r.front {
			// fmt.Println(" has next", r.front, "-", r.back)

			// 다음 영역과 병합
			if !merge {
				// fmt.Println(" overlapped to next")
				return r, ErrOverlapped
			}

			r.front = front
			r.back = maxInt64(back, r.back)

			// fmt.Println(" merged to next", r.front, "-", r.back)

			removeCount := 0

			// 이전 영역과 병합
			prevRange, _ := s.mergeToPrevious(
				nextIndex, length, r.front, r.back, merge)
			if prevRange != nil {
				// 병합되었으면 삭제할 영역 개수 증가
				removeCount++
				r = prevRange
			}

			// 추가로 병합할 다음 영역 처리
			for index := nextIndex + removeCount; index < length; index++ {
				nextRange := s.slice[index]

				if r.back >= nextRange.front {
					// fmt.Println(" has next", nextRange.front, "-", nextRange.back)

					r.back = maxInt64(r.back, nextRange.back)

					// fmt.Println(" merged", r.front, "-", r.back)

					removeCount++
				} else {
					break
				}
			}

			s.slice.removeIndex(nextIndex, removeCount)
			// fmt.Println(" removed", nextIndex, "count", removeCount)

			return
		}
		// 다음 영역과 겹치지 않음
	} else {
		// 다음 영역이 없음
	}

	// 이전 영역과 병합 시도
	r, err = s.mergeToPrevious(nextIndex, length, front, back, merge)
	if r != nil { // 병합 노드가 있으면 병합되었거나 오류 발생이므로 반환
		return
	}

	// 새로운 영역 추가
	r = newRange(front, back)
	if length > 0 && back < s.slice[0].front {
		s.slice.insertAtIndex(0, r)
	} else {
		s.slice.insertAtIndex(nextIndex, r)
	}
	// fmt.Println(" add new", r.front, "-", r.back)

	return
}

func (s *RangeSet) mergeToPrevious(index int, length int, front, back int64, merge bool) (*Range, error) {
	prevIndex := index - 1
	if prevIndex < 0 {
		return nil, nil
	}

	prevRange := s.slice[prevIndex]
	if front <= prevRange.back {
		// fmt.Println(" has prev", prevRange.front, "-", prevRange.back)

		if merge {
			prevRange.back = back

			// fmt.Println(" merged to prev", prevRange.front, "-", prevRange.back)

			return prevRange, nil
		}
		// fmt.Println(" overlapped to prev")
		return prevRange, ErrOverlapped
	}
	return nil, nil
}

// Remove -- 영역 제거
func (s *RangeSet) Remove(front, back int64, force bool) error {
	// fmt.Println("remove", front, "-", back)
	nextIndex, length := s.slice.search(front) // 같거나 다음 위치의 영역 찾기
	if nextIndex < length {
		if !force {
			r := s.slice[nextIndex]
			if r.front == front && r.back == back {
				// 정확한 영역 지정
				s.slice.removeIndex(nextIndex, 1)
				// fmt.Println(" deleted", r.front, "-", r.back)
				return nil
			}
			// fmt.Println(" overlapped?")
			return ErrNotFound
		}

		removeCount := 0
		for index := nextIndex; index < length; index++ {
			r := s.slice[index]
			if r.front <= back {
				// fmt.Println(" has next", r.front, "-", r.back)
				if r.back <= back {
					removeCount++
				} else {
					r.front = back + 1
					// fmt.Println(" cutted", r.front, "-", r.back)
				}
			} else {
				break
			}
		}
		s.slice.removeIndex(nextIndex, removeCount)
		// fmt.Println(" removed", nextIndex, "count", removeCount)
	} else {
		// 다음 영역이 없음
	}

	// 이전 영역 처리
	index := nextIndex - 1
	if index != -1 {
		r := s.slice[index]
		if r.back >= front {
			// fmt.Println(" has prev", r.front, "-", r.back)
			if r.back > back {
				// 두개로 자르기
				n := newRange(back+1, r.back)
				s.slice.insertAtIndex(index+1, n)

				// fmt.Println(" splited",
				// 	r.front, "-", r.back, ",",
				// 	n.front, "-", n.back)
			}
			r.back = front - 1
			// fmt.Println(" cutted", r.front, "-", r.back)
		}
	}
	return nil
}

func (s RangeSet) String() string {
	var sb strings.Builder
	for _, r := range s.slice {
		sb.WriteString("[")
		sb.WriteString(strconv.FormatInt(r.Front(), 10))
		sb.WriteString("-")
		sb.WriteString(strconv.FormatInt(r.Back(), 10))
		sb.WriteString("],")
	}
	return sb.String()
}
