package rangeset

import "sort"

type rangeSlice []*Range

func newRanges(len, cap int) rangeSlice {
	return make([]*Range, len, cap)
}

// -- sort 패키지 사용을 위한 메소드

// Len -- 길이
func (r rangeSlice) Len() int {
	return len(r)
}

// Less -- 순서 비교
func (r rangeSlice) Less(i, j int) bool {
	return r[i].front < r[j].front
}

// Swap -- 데이터 교환
func (r rangeSlice) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

// -- 배열 제어를 위한 메소드

func (r rangeSlice) search(front int64) (i int, length int) {
	length = len(r)
	i = sort.Search(length, func(i int) bool {
		return r[i].front >= front
	})
	return
}

func (r *rangeSlice) removeIndex(i int, count int) {
	if count > 0 {
		*r = append((*r)[:i], (*r)[i+count:]...)
	}
}

func (r *rangeSlice) insertAtIndex(i int, v *Range) {
	if i == len(*r) {
		r.append(v)
	} else {
		*r = append(*r)
		copy((*r)[i+1:], (*r)[i:])
		(*r)[i] = v
	}
}

func (r *rangeSlice) append(v *Range) {
	*r = append(*r, v)
}
