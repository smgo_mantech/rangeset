package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"rangeset/rangeset"
)

func addTest(set *rangeset.RangeSet, fileSize, blockSize, count int64) *sync.WaitGroup {
	wg := new(sync.WaitGroup)
	wg.Add(1)

	go func() {
		s := rand.NewSource(time.Now().UnixNano())
		r := rand.New(s)

		addedCount := 0
		mergedCount := 0
		lastLen := 0

		for count > 0 {
			offset := r.Int63n(fileSize - blockSize)
			r, err := set.AddOffsetSize(offset, blockSize, true)
			if err != nil {
				fmt.Println("ERROR:", err, "offset:", offset, "size:", blockSize)
			}
			if r.Back() < r.Front() {
				fmt.Println("WRONG DATA:", r.Front(), "-", r.Back())
			}

			l := set.Len()
			if lastLen+1 == l {
				addedCount++
			} else {
				mergedCount++
			}
			lastLen = set.Len()

			prevB := int64(0)
			for _, r := range set.Array() {
				f := r.Front()
				b := r.Back()
				if r.Front() <= prevB {
					fmt.Println("DATA OVERLAPPED prevBack:", prevB, "front:", f)
					fmt.Println("ADD front:", offset, "back:", blockSize+offset-1)
					fmt.Println("AFTER: ", set.String())

					wg.Done()
					return
				}
				prevB = b
			}

			count--
		}
		fmt.Println("added:", addedCount, ", merged:", mergedCount)
		wg.Done()
	}()

	return wg
}

func main() {
	fileSize := int64(10) * 1024 * 1024 * 1024 // 10 GB
	blockSize := int64(1) * 1024 * 1024        // 1 MB
	count := fileSize / blockSize

	set := rangeset.New(1000)

	started := time.Now()
	testWaiter := addTest(set, fileSize, blockSize, count)
	testWaiter.Wait()
	elapsed := time.Since(started)
	elapsedPerAction := elapsed / time.Duration(count)

	fmt.Println("elapsed:", elapsed.String(), ", per action:", elapsedPerAction.String())
	fmt.Println("action count:", count)
	fmt.Println("range count:", set.Len())

	arr := set.Array()
	fmt.Println("front:", arr[0].Front())
	fmt.Println("back: ", arr[len(arr)-1].Back())
}
